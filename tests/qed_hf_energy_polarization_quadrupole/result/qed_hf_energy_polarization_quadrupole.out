


                     eT 1.7 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   E. RoncaM. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. 
   H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.7.0 Hydra
  ------------------------------------------------------------
  Configuration date: 2022-04-07 12:33:31 UTC -07:00
  Git branch:         release-v1.7.0
  Git hash:           c5c382d7d23365463896a87c37298e4a37e4c962
  Fortran compiler:   GNU 11.2.0
  C compiler:         GNU 11.2.0
  C++ compiler:       GNU 11.2.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2022-04-07 12:34:43 UTC -07:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: HOF-He
        charge: 0
     end system

     do
        ground state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     method
        qed-hf
     end method

     qed
        modes:        1
        frequency:    {0.5}
        polarization: {0, 0, 1}
        coupling:     {0.05}
        quadrupole oei
     end qed


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: QED-RHF wavefunction
  ==========================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  F    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.000000000000     0.000000000000     7.500000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  F    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.000000000000     0.000000000000    14.172945934238        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               38
     Number of orthonormal atomic orbitals:   38

  - Molecular orbital details:

     Number of occupied orbitals:        10
     Number of virtual orbitals:         28
     Number of molecular orbitals:       38


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a QED-RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:              -178.309168178748
     Number of electrons in guess:           20.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -177.445285735597     0.7569E-01     0.1774E+03
     2          -177.471934886254     0.1126E-01     0.2665E-01
     3          -177.472931078298     0.4110E-02     0.9962E-03
     4          -177.473059151767     0.1363E-02     0.1281E-03
     5          -177.473072665228     0.4338E-03     0.1351E-04
     6          -177.473074017753     0.8034E-04     0.1353E-05
     7          -177.473074099747     0.2288E-04     0.8199E-07
     8          -177.473074109886     0.4798E-05     0.1014E-07
     9          -177.473074110712     0.1042E-05     0.8267E-09
    10          -177.473074110743     0.2401E-06     0.3052E-10
    11          -177.473074110744     0.5227E-07     0.6821E-12
    12          -177.473074110744     0.9528E-08     0.1421E-12
    13          -177.473074110744     0.3942E-08     0.8527E-13
    14          -177.473074110744     0.1825E-08     0.1421E-12
    15          -177.473074110744     0.6790E-09     0.1421E-12
    16          -177.473074110744     0.1509E-09     0.8527E-13
    17          -177.473074110744     0.4928E-10     0.1705E-12
  ---------------------------------------------------------------
  Convergence criterion met in 17 iterations!

  - Summary of QED-RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.602720772886
     Nuclear repulsion energy:      48.518317619727
     Electronic energy:           -225.991391730471
     Total energy:                -177.473074110744

  - QED Parameters and properties

     Optimize photons?   :    True
     Complete basis?     :    True

     Mode 1
      Frequency          :    0.500000000000
      Polarization       :    0.000000000000    0.000000000000    1.000000000000
      Coupling
       sqrt(1/eps V)     :    0.050000000000
       Bilinear          :    0.025000000000
       Quadratic         :    0.001250000000
      Coherent state     :   -0.000149513693

  - Timings for the QED-RHF ground state calculation

     Total wall time (sec):              1.06800
     Total cpu time (sec):               1.03894

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 434.788 KB

  Total wall time in eT (sec):              1.07900
  Total cpu time in eT (sec):               1.05099

  Calculation ended: 2022-04-07 12:34:44 UTC -07:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713
     QED-HF: https://doi.org/10.1103/PhysRevX.10.041043

  eT terminated successfully!
