


                     eT 1.7 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   E. RoncaM. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. 
   H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.7.0 Hydra
  ------------------------------------------------------------
  Configuration date: 2022-04-07 12:33:31 UTC -07:00
  Git branch:         release-v1.7.0
  Git hash:           c5c382d7d23365463896a87c37298e4a37e4c962
  Fortran compiler:   GNU 11.2.0
  C compiler:         GNU 11.2.0
  C++ compiler:       GNU 11.2.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2022-04-07 12:34:37 UTC -07:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O-H2O-tip3p
        charge: 0
     end system

     do
       ground state
     end do


     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     molecular mechanics
        forcefield: non-polarizable
     end molecular mechanics

     method
        hf
     end method


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: aug-cc-pvdz
        1  H     0.023452000000     0.185621000000     0.000000000000        1
        2  H     0.906315000000     1.422088000000     0.000000000000        2
        3  O     0.009319000000     1.133156000000     0.000000000000        3
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: aug-cc-pvdz
        1  H     0.044317857073     0.350772852968     0.000000000000        1
        2  H     1.712687132585     2.687356845030     0.000000000000        2
        3  O     0.017610357755     2.141354496408     0.000000000000        3
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               41
     Number of orthonormal atomic orbitals:   41

  This is a QM/MM calculation

     Embedding type: non-polarizable

     Each atom of the MM portion is endowed with a charge which value 
     is a fixed external parameter.

     The QM/MM electrostatic interaction energy is defined as:

        E^ele_QM/MM = sum_i q_i * V_i(P)

     where V_i(P) is the electrostatic potential due to the QM density 
     calculated at the position of the i-th charge q_i.

     For further details, see:
     Senn & Thiel, Angew. Chem. Int. Ed., 2009, 48, 1198−1229


     ====================================================================
                             MM Geometry (angstrom)
     ====================================================================
     Atom                    X          Y          Z            Charge
     ====================================================================
      O                -0.042964  -1.404707  -0.000000       -0.834000
      H                -0.419020  -1.818953   0.760190        0.417000
      H                -0.419020  -1.818953  -0.760190        0.417000
     ====================================================================

     ====================================================================
                             MM Geometry (a.u.)
     ====================================================================
     Atom                    X          Y          Z            Charge
     ====================================================================
      O                -0.081190  -2.654512  -0.000000       -0.834000
      H                -0.791833  -3.437323   1.436551        0.417000
      H                -0.791833  -3.437323  -1.436551        0.417000
     ====================================================================

  - Molecular orbital details:

     Number of occupied orbitals:         5
     Number of virtual orbitals:         36
     Number of molecular orbitals:       41


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:               -76.187335827224
     Number of electrons in guess:           10.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -76.341900642499     0.1058E+00     0.7634E+02
     2           -76.374795609177     0.6986E-01     0.3289E-01
     3           -76.390791803515     0.3770E-02     0.1600E-01
     4           -76.390957192558     0.1071E-02     0.1654E-03
     5           -76.390968733138     0.1696E-03     0.1154E-04
     6           -76.390969287752     0.3498E-04     0.5546E-06
     7           -76.390969304686     0.5524E-05     0.1693E-07
     8           -76.390969305099     0.6796E-06     0.4131E-09
     9           -76.390969305103     0.4361E-07     0.4121E-11
    10           -76.390969305103     0.4902E-08     0.1137E-12
    11           -76.390969305103     0.1421E-08     0.2842E-13
    12           -76.390969305103     0.3104E-09     0.2842E-13
    13           -76.390969305103     0.6708E-10     0.0000E+00
  ---------------------------------------------------------------
  Convergence criterion met in 13 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.517490047373
     Nuclear repulsion energy:       9.307879526626
     Electronic energy:            -85.698848831729
     Total energy:                 -76.390969305103

  - Summary of QM/MM energetics:
                                         a.u.             eV     kcal/mol
     QM/MM SCF Contribution:         0.323022340863     8.78989   202.700
     QM/MM Electrostatic Energy:    -0.350876629345    -9.54784  -220.178

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.73100
     Total cpu time (sec):               0.63336

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 370.836 KB

  Total wall time in eT (sec):              0.75000
  Total cpu time in eT (sec):               0.64654

  Calculation ended: 2022-04-07 12:34:37 UTC -07:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
