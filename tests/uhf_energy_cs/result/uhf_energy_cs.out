


                     eT 1.7 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   E. RoncaM. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. 
   H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.7.0 Hydra
  ------------------------------------------------------------
  Configuration date: 2022-04-07 12:33:31 UTC -07:00
  Git branch:         release-v1.7.0
  Git hash:           c5c382d7d23365463896a87c37298e4a37e4c962
  Fortran compiler:   GNU 11.2.0
  C compiler:         GNU 11.2.0
  C++ compiler:       GNU 11.2.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2022-04-07 12:34:39 UTC -07:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: Cs
        charge: 0
        multiplicity: 2
     end system

     do
        ground state
     end do

     method
        uhf
     end method

     memory
        available: 8
     end memory

     solver scf
       algorithm:          scf-diis
       energy threshold:   1.0d-10
       gradient threshold: 1.0d-10
     end solver scf


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: UHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 3-21g
        1 Cs     0.000000000000     0.000000000000     0.000000000000        1
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 3-21g
        1 Cs     0.000000000000     0.000000000000     0.000000000000        1
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               37
     Number of orthonormal atomic orbitals:   37

  - Molecular orbital details:

     Number of alpha electrons:              28
     Number of beta electrons:               27
     Number of virtual alpha orbitals:        9
     Number of virtual beta orbitals:        10
     Number of molecular orbitals:           37


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a UHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:             -7530.568190922048
     Number of electrons in guess:           55.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1         -7530.542527047884     0.4281E-01     0.7531E+04
     2         -7530.543242039053     0.1751E-02     0.7150E-03
     3         -7530.543343909523     0.6922E-03     0.1019E-03
     4         -7530.543363599997     0.6722E-04     0.1969E-04
     5         -7530.543363805163     0.7224E-05     0.2052E-06
     6         -7530.543363806804     0.3314E-05     0.1642E-08
     7         -7530.543363807132     0.2769E-06     0.3274E-09
     8         -7530.543363807135     0.1487E-07     0.3638E-11
     9         -7530.543363807137     0.2639E-09     0.1819E-11
    10         -7530.543363807135     0.4195E-10     0.1819E-11
  ---------------------------------------------------------------
  Convergence criterion met in 10 iterations!

  - Summary of UHF wavefunction energetics (a.u.):

     HOMO-LUMO gap (alpha):          0.289543124728
     HOMO-LUMO gap (beta):           0.696523627897
     Nuclear repulsion energy:       0.000000000000
     Electronic energy:          -7530.543363807135
     Total energy:               -7530.543363807135

  - UHF wavefunction spin expectation values:

     Sz:                   0.50000000
     Sz(Sz + 1):           0.75000000
     S^2:                  0.75022793
     Spin contamination:   0.00022793

  - Timings for the UHF ground state calculation

     Total wall time (sec):              3.17300
     Total cpu time (sec):               2.43024

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 488.300 KB

  Total wall time in eT (sec):              3.18800
  Total cpu time in eT (sec):               2.43684

  Calculation ended: 2022-04-07 12:34:42 UTC -07:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
